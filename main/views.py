from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.

def index(request):
    return render(request, 'x.html')

def access_json(request):
    json_url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
    json_books = requests.get(json_url).json()
    return JsonResponse(json_books)